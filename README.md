# painting-ants
Some ants painting not so random stuff


# Optimisation du code 

## Branche movingAnts_opti

### deplacer()
* **Modification** : 
Dans la classe Fourmis : retrait de `synchronized` sur la fonction `deplacer()` 
les fourmis n'étant pas des threads et n'accédant pas à des ressources partagées des threads,
les objets n'ont pas besoins d'avoir de méthode `synchronized`


* **Résultat** : Augmentation du nombre de fourmis par seconde

## Branche opti_color

### ColorBuffer
* **Modification** : 
Création d'un tableau de dimension 16_777_217 pour contenir les couleurs des fourmis.
Les couleurs ne sont instanciées qu'une seule fois si elle existe déjà

* **Résultat** : Diminution du nombre de fourmis par seconde 


## Branche paintingAnts_opti

### lMessage

* **Modification** : 
    Modification de la classe paintingAnts dans la fonction run() en passant lMessage  de String à StringBuilder;

* **Résultat** : 
    Augmentation du nombre de fourmis

### mCompteur 
* **Modification** : du mCompteur de la classe paintingAnts en passant de long à AtomicLong afin de supprimer des `synchronized`;

* **Résultat** :  Augmentation du nombre de fourmis

